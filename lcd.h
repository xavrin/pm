#ifndef __LCD__H__
#define __LCD__H__

void LCDconfigure();
void LCDclear();
void LCDdrawBoard(char board[SIZEX][SIZEY]);
void LCDdrawPoints(int points);

#endif
