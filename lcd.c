#include <delay.h>
#include "sleep.h"
#include <fonts.h>
#include <gpio.h>
#include <lcd.h>
#include <lcd_board_def.h>
#include "board.h"
#include "utils.h"

/* Microcontroller pin definitions:
constants LCD_*_GPIO_N are port letter codes (A, B, C, ...),
constants LCD_*_PIN_N are the port output numbers (from 0 to 15),
constants GPIO_LCD_* are memory pointers,
constants PIN_LCD_* and RCC_LCD_* are bit masks. */

#define GPIO_LCD_CS   xcat(GPIO, LCD_CS_GPIO_N)
#define GPIO_LCD_A0   xcat(GPIO, LCD_A0_GPIO_N)
#define GPIO_LCD_SDA  xcat(GPIO, LCD_SDA_GPIO_N)
#define GPIO_LCD_SCK  xcat(GPIO, LCD_SCK_GPIO_N)

#define PIN_LCD_CS    (1U << LCD_CS_PIN_N)
#define PIN_LCD_A0    (1U << LCD_A0_PIN_N)
#define PIN_LCD_SDA   (1U << LCD_SDA_PIN_N)
#define PIN_LCD_SCK   (1U << LCD_SCK_PIN_N)

#define RCC_LCD_CS    xcat3(RCC_AHB1ENR_GPIO, LCD_CS_GPIO_N, EN)
#define RCC_LCD_A0    xcat3(RCC_AHB1ENR_GPIO, LCD_A0_GPIO_N, EN)
#define RCC_LCD_SDA   xcat3(RCC_AHB1ENR_GPIO, LCD_SDA_GPIO_N, EN)
#define RCC_LCD_SCK   xcat3(RCC_AHB1ENR_GPIO, LCD_SCK_GPIO_N, EN)

/* Screen size in pixels, left top corner has coordinates (0, 0). */

#define LCD_PIXEL_WIDTH   128
#define LCD_PIXEL_HEIGHT  160

/* Some color definitions */

#define LCD_COLOR_WHITE    0xFFFF
#define LCD_COLOR_BLACK    0x0000
#define LCD_COLOR_GREY     0xF7DE
#define LCD_COLOR_BLUE     0x001F
#define LCD_COLOR_BLUE2    0x051F
#define LCD_COLOR_RED      0xF800
#define LCD_COLOR_MAGENTA  0xF81F
#define LCD_COLOR_GREEN    0x07E0
#define LCD_COLOR_CYAN     0x7FFF
#define LCD_COLOR_YELLOW   0xFFE0

/* Needed delay(s)  */

#define Tinit   150
#define T120ms  (MAIN_CLOCK_MHZ * 120000 / 4)

#define BACK_COLOR LCD_COLOR_GREEN
#define TEXT_COLOR LCD_COLOR_BLACK
#define BOARD_COLOR LCD_COLOR_WHITE
#define ROCK_COLOR LCD_COLOR_RED
#define BATTLESHIP_COLOR LCD_COLOR_BLUE
#define BULLET_COLOR LCD_COLOR_BLACK

#define MAX_GREEN 5
int greens[] = {0x05E0, 0x06E0, 0x07E0, 0x04E0, 0x03E0};

/** Internal functions **/

/* The following four functions are inlined and "if" statement is
eliminated during optimization if the "bit" argument is a constant. */

#define OFFSETY 18
#define OFFSETX 0
#define CNT_OFFSETX 0
#define CNT_OFFSETY 0
#define DEFAULT_FONT font10x18;
#define BSIZEX (2 * SIZEX)
#define BSIZEY (2 * SIZEY)

static const font_t *CurrentFont;

static void CS(uint32_t bit) {
  if (bit) {
    GPIO_LCD_CS->BSRRL = PIN_LCD_CS; /* Activate chip select line. */
  }
  else {
    GPIO_LCD_CS->BSRRH = PIN_LCD_CS; /* Deactivate chip select line. */
  }
}

static void A0(uint32_t bit) {
  if (bit) {
    GPIO_LCD_A0->BSRRL = PIN_LCD_A0; /* Set data/command line to data. */
  }
  else {
    GPIO_LCD_A0->BSRRH = PIN_LCD_A0; /* Set data/command line to command. */
  }
}

static void SDA(uint32_t bit) {
  if (bit) {
    GPIO_LCD_SDA->BSRRL = PIN_LCD_SDA; /* Set data bit one. */
  }
  else {
    GPIO_LCD_SDA->BSRRH = PIN_LCD_SDA; /* Set data bit zero. */
  }
}

static void SCK(uint32_t bit) {
  if (bit) {
    GPIO_LCD_SCK->BSRRL = PIN_LCD_SCK; /* Rising clock edge. */
  }
  else {
    GPIO_LCD_SCK->BSRRH = PIN_LCD_SCK; /* Falling clock edge. */
  }
}

static void RCCconfigure() {
  /* Enable GPIO clocks. */
  RCC->AHB1ENR |= RCC_LCD_CS | RCC_LCD_A0 | RCC_LCD_SDA | RCC_LCD_SCK;
}

static void GPIOconfigure() {
  CS(1); /* Set CS inactive. */
  GPIOoutConfigure(GPIO_LCD_CS, LCD_CS_PIN_N, GPIO_OType_PP,
                   GPIO_High_Speed, GPIO_PuPd_NOPULL);

  A0(1); /* Data are sent default. */
  GPIOoutConfigure(GPIO_LCD_A0, LCD_A0_PIN_N, GPIO_OType_PP,
                   GPIO_High_Speed, GPIO_PuPd_NOPULL);

  SDA(0);
  GPIOoutConfigure(GPIO_LCD_SDA, LCD_SDA_PIN_N, GPIO_OType_PP,
                   GPIO_High_Speed, GPIO_PuPd_NOPULL);

  SCK(0); /* Data bit is written on rising clock edge. */
  GPIOoutConfigure(GPIO_LCD_SCK, LCD_SCK_PIN_N, GPIO_OType_PP,
                   GPIO_High_Speed, GPIO_PuPd_NOPULL);
}

static void LCDwriteSerial(uint32_t data, uint32_t length) {
  uint32_t mask;

  mask = 1U << (length - 1);
  while (length > 0) {
    SDA(data & mask); /* Set bit. */
    --length;         /* Add some delay. */
    SCK(1);           /* Rising edge writes bit. */
    mask >>= 1;       /* Add some delay. */
    SCK(0);           /* Falling edge ends the bit transmission. */
  }
}

static void LCDwriteCommand(uint32_t data) {
  A0(0);
  LCDwriteSerial(data, 8);
  A0(1);
}

static void LCDwriteData8(uint32_t data) {
  /* A0(1); is already set */
  LCDwriteSerial(data, 8);
}

static void LCDwriteData16(uint32_t data) {
  /* A0(1); is already set */
  LCDwriteSerial(data, 16);
}

static void LCDwriteData24(uint32_t data) {
  /* A0(1); is already set */
  LCDwriteSerial(data, 24);
}

static void LCDwriteData32(uint32_t data) {
  /* A0(1); is already set */
  LCDwriteSerial(data, 32);
}

void LCDsetRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2) {
  LCDwriteCommand(0x2A);
  LCDwriteData16(x1);
  LCDwriteData16(x2);

  LCDwriteCommand(0x2B);
  LCDwriteData16(y1);
  LCDwriteData16(y2);

  LCDwriteCommand(0x2C);
}

static void LCDcontrollerConfigure() {
  /* Activate chip select */
  CS(0);

  Delay(Tinit);

  /* Sleep out */
  LCDwriteCommand(0x11);

  Delay(T120ms);

  /* Frame rate */
  LCDwriteCommand(0xB1);
  LCDwriteData24(0x053C3C);
  LCDwriteCommand(0xB2);
  LCDwriteData24(0x053C3C);
  LCDwriteCommand(0xB3);
  LCDwriteData24(0x053C3C);
  LCDwriteData24(0x053C3C);

  /* Dot inversion */
  LCDwriteCommand(0xB4);
  LCDwriteData8(0x03);

  /* Power sequence */
  LCDwriteCommand(0xC0);
  LCDwriteData24(0x280804);
  LCDwriteCommand(0xC1);
  LCDwriteData8(0xC0);
  LCDwriteCommand(0xC2);
  LCDwriteData16(0x0D00);
  LCDwriteCommand(0xC3);
  LCDwriteData16(0x8D2A);
  LCDwriteCommand(0xC4);
  LCDwriteData16(0x8DEE);

  /* VCOM */
  LCDwriteCommand(0xC5);
  LCDwriteData8(0x1A);

  /* Memory and color write direction */
  LCDwriteCommand(0x36);
  LCDwriteData8(0xC0);

  /* Color mode 16 bit per pixel */
  LCDwriteCommand(0x3A);
  LCDwriteData8(0x05);

  /* Gamma sequence */
  LCDwriteCommand(0xE0);
  LCDwriteData32(0x0422070A);
  LCDwriteData32(0x2E30252A);
  LCDwriteData32(0x28262E3A);
  LCDwriteData32(0x00010313);
  LCDwriteCommand(0xE1);
  LCDwriteData32(0x0416060D);
  LCDwriteData32(0x2D262327);
  LCDwriteData32(0x27252D3B);
  LCDwriteData32(0x00010413);

  /* Display on */
  LCDwriteCommand(0x29);

  /* Deactivate chip select */
  CS(1);
}

static void LCDdrawChar(int no, unsigned c) {
    uint16_t const *p;
    uint16_t x, y, w;
    int      i, j;

    CS(0);

    y = CNT_OFFSETY;
    x = CNT_OFFSETX + no * CurrentFont->width;
    LCDsetRectangle(x, y, x + CurrentFont->width - 1, y + CurrentFont->height - 1);
    p = &CurrentFont->table[(c - FIRST_CHAR) * CurrentFont->height];

    for (i = 0; i < CurrentFont->height; ++i) {
        for (j = 0, w = p[i]; j < CurrentFont->width; ++j, w >>= 1) {
            LCDwriteData16(w & 1 ? TEXT_COLOR : BACK_COLOR);
        }
    }

    CS(1);
}

/** Public interface implementation **/
void LCDconfigure() {
  /* See Errata, 2.1.6 Delay after an RCC peripheral clock enabling */
  RCCconfigure();
  CurrentFont = &DEFAULT_FONT;
  /* Initialize hardware. */
  GPIOconfigure();
  LCDcontrollerConfigure();
  LCDclear();
}

void LCDclear() {
  int i, j;

  CS(0);
  LCDsetRectangle(0, 0, LCD_PIXEL_WIDTH - 1, LCD_PIXEL_HEIGHT - 1);
  for (i = 0; i < LCD_PIXEL_WIDTH; ++i) {
    for (j = 0; j < LCD_PIXEL_HEIGHT; ++j) {
      LCDwriteData16(BACK_COLOR);
    }
  }
  CS(1);
}

void LCDdrawBoard(char board[SIZEX][SIZEY]) {
    int i, j;
    CS(0);
    LCDsetRectangle(OFFSETX, OFFSETY, OFFSETX + BSIZEX - 1, OFFSETY + BSIZEY - 1);
    for (j = BSIZEY - 1; j >= 0; j--)
        for (i = 0; i < BSIZEX; i++) {
            int color;
            switch (board[i / 2][j / 2]) {
                case BOARDC:
                    color = BOARD_COLOR;
                    break;
                case BATTLESHIPC:
                    color = BATTLESHIP_COLOR;
                    break;
                case BULLETC:
                    color = BULLET_COLOR;
                    break;
                default:
                    color = greens[board[i / 2][j / 2] % MAX_GREEN];
                    break;
            }
            LCDwriteData16(color);
        }
    CS(1);
}

void LCDdrawPoints(int points) {
    unsigned char digits[20];
    int i = 0;
    int no = 0;

    if (points < 0) {
        LCDdrawChar(no++, '-');
        points = -points;
    }

    while (points) {
        digits[i] = (points % 10) + '0';
        points /= 10;
        i++;
    }

    if (i)
        i--;
    else
        digits[0] = '0';

    while (i >= 0) {
        LCDdrawChar(no, digits[i]);
        i--;
        no++;
    }
}
