#ifndef __MAIN_TIMER__H__
#define __MAIN_TIMER__H__

/* Configure the main timer.
 * Handler will be called every time the timer reaches max. */
void configure_main_timer(void (*handler)(void));
void TIM3_IRQHandler(void);

#endif
