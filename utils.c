#include <stm32.h>
#include <gpio.h>
#include "utils.h"

/* constants for USART */
#define GPIO_AF_USART2 7
#define USART_Mode_Rx_Tx (USART_CR1_RE | USART_CR1_TE)
#define USART_Enable USART_CR1_UE
#define USART_WordLength_8b 0x0000
#define USART_WordLength_9b USART_CR1_M
#define USART_Parity_No 0x0000
#define USART_Parity_Even USART_CR1_PCE
#define USART_Parity_Odd (USART_CR1_PCE | USART_CR1_PS)
#define USART_StopBits_1 0x0000
#define USART_StopBits_0_5 0x1000
#define USART_StopBits_2 0x2000
#define USART_StopBits_1_5 0x3000
#define USART_FlowControl_None 0x0000
#define USART_FlowControl_RTS USART_CR3_RTSE
#define USART_FlowControl_CTS USART_CR3_CTSE
#define HSI_VALUE 16000000

void write_one(char c) {
    while (!(USART2->SR & USART_SR_TXE));
    USART2->DR = c;
}

void write_n(char * ptr, unsigned len) {
    unsigned i;
    for (i = 0; i < len; i++)
        write_one(ptr[i]);
}

void write(const char *ptr) {
    while (*ptr) {
        write_one(*ptr);
        ptr++;
    }
    write_one('\n');
    write_one('\r');
}

inline irq_level_t IRQprotectAll() {
    irq_level_t level;
    level = __get_PRIMASK();
    __disable_irq();
    return level;
}

inline void IRQunprotectAll(irq_level_t level) {
    __set_PRIMASK(level);
}

void init_usart() {
    unsigned baudrate = 9600;
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
    __NOP();
    GPIOafConfigure(GPIOA,
                    2U,
                    GPIO_OType_PP,
                    GPIO_Fast_Speed,
                    GPIO_PuPd_NOPULL,
                    GPIO_AF_USART2);
    GPIOafConfigure(GPIOA,
                    3U,
                    GPIO_OType_PP,
                    GPIO_Fast_Speed,
                    GPIO_PuPd_UP,
                    GPIO_AF_USART2);
    USART2->CR1 = USART_Mode_Rx_Tx | USART_WordLength_8b | USART_Parity_No;
    USART2->CR2 = USART_StopBits_1;
    USART2->CR3 = USART_FlowControl_None;

    USART2->BRR = (HSI_VALUE + (baudrate / 2U)) / baudrate;
    USART2->CR1 |= USART_Enable;
}
