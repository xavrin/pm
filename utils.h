#ifndef __UTILS__H__
#define __UTILS__H__


/* Write one character to USART */
void write_one(char c);

/* Write len characters to USART starting from ptr. */
void write_n(char * ptr, unsigned len);

/* Write a null-terminated string ptr. */
void write(const char *ptr);

void init_usart();


typedef uint32_t irq_level_t;

/* Disable interrupts. */
inline irq_level_t IRQprotectAll();

/* Enable interrupts. */
inline void IRQunprotectAll(irq_level_t level);

#endif
