#ifndef __GAME__H__
#define __GAME__H__

#include "board.h"

#define MAX_ROCKS 30 /* maximal number of rocks on the board */
#define MAX_BULLETS ((SIZEX * SIZEY) / 4) /* maximal number of bullets on the
                                             board */
#define SHOOTING_ROCK_POINTS 10 /* number of points for shooting a single
                                   rock */

#define BXSIZE 5 /* width of the battleship */
#define BYSIZE 1 /* height of the battleship */

/* starting position of the bullet relative to the left bottom corner of the
 * battleship */
#define FIREPOSX 2
#define FIREPOSY 1

/* Size of all the rocks. */
#define RXSIZE 8
#define RYSIZE 8

/* Frequencies of the moves for the objects.
 * N means that an object will move every N turns. */
#define BATTLESHIP_STEP 1
#define BULLET_STEP 1
#define ROCK_STEP_MIN 4
#define ROCK_STEP_MAX 10

/* Direction of moving. */
typedef enum {
    UP, DOWN, LEFT, RIGHT, NONE
} Dir;

typedef enum {
    LOST, PLAYING
} GameStatus;

typedef struct {
    char x, y;
} Pos;

typedef struct {
    char x, y;
} Size;

typedef struct {
    char val;
    char max;
} Speed;

typedef struct {
    Pos pos;
    Size size;
    Speed speed;
    char destroyed;
    int color;
} Rock;

typedef struct {
    Pos pos;
    Speed speed;
} Bullet;

typedef struct {
    Pos pos;
    Size size;
    Dir dir;
    Speed speed;
} Battleship;

typedef struct {
    Battleship b;
    Rock rocks[MAX_ROCKS];
    Bullet bullets[MAX_BULLETS];
    char rocks_num;
    int bullets_num;
    int points;
    char fired;
    GameStatus status;
} Game;

void init_game(Game *game);
void move_left(Game *game); /* move battleship to the left */
void move_right(Game *game); /* move battleship to the right */
void fire_bullet(Game *game); /* fire a bullet from the battleship */
void make_step(Game *game); /* make a time step in the game */
void draw_board(Game *game, char board[SIZEX][SIZEY]);
int get_points(Game *game);

#endif
