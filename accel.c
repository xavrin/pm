#include <stm32.h>
#include <gpio.h>
#include "accel.h"
#include "utils.h"

/* constants for the accelerometer */
#define LIS35DE_ADDR (0x1D)

#define OUTX 0x29
#define OUTY 0x2B
#define OUTZ 0x2D

#define CTRL_REG1 0x20
#define CTRL_REG1_DR (1 << 7)
#define CTRL_REG1_PD (1 << 6)
#define CTRL_REG1_FS (1 << 5)
#define CTRL_REG1_Zen (1 << 2)
#define CTRL_REG1_Yen (1 << 1)
#define CTRL_REG1_Xen (1 << 0)

#define CTRL_REG3 0x22
#define CTRL_REG3_I1CFG0 (1 << 0)
#define CTRL_REG3_I1CFG1 (1 << 1)
#define CTRL_REG3_I1CFG2 (1 << 2)
#define CTRL_REG3_I2CFG0 (1 << 3)
#define CTRL_REG3_I2CFG1 (1 << 4)
#define CTRL_REG3_I2CFG2 (1 << 5)
#define CTRL_REG3_PP_OD  (1 << 6)
#define CTRL_REG3_IHL    (1 << 7)

#define FF_WU_CFG_1 0x30
#define FF_WU_CFG_1_XLIE (1 << 0)
#define FF_WU_CFG_1_XHIE (1 << 1)
#define FF_WU_CFG_1_YLIE (1 << 2)
#define FF_WU_CFG_1_YHIE (1 << 3)
#define FF_WU_CFG_1_ZLIE (1 << 4)
#define FF_WU_CFG_1_ZHIE (1 << 5)
#define FF_WU_CFG_1_LIR (1 << 6)
#define FF_WU_CFG_1_AOI (1 << 7)

#define FF_WU_SRC_1 0x31
#define FF_WU_SRC_1_XL (1 << 0)
#define FF_WU_SRC_1_XH (1 << 1)
#define FF_WU_SRC_1_YL (1 << 2)
#define FF_WU_SRC_1_YH (1 << 3)
#define FF_WU_SRC_1_ZL (1 << 4)
#define FF_WU_SRC_1_ZH (1 << 5)
#define FF_WU_SRC_1_IA (1 << 6)

#define FF_WU_THS_1 0x32
#define FF_WU_THS_1_THS0 (1 << 0)
#define FF_WU_THS_1_THS1 (1 << 1)
#define FF_WU_THS_1_THS2 (1 << 2)
#define FF_WU_THS_1_THS3 (1 << 3)
#define FF_WU_THS_1_THS4 (1 << 4)
#define FF_WU_THS_1_THS5 (1 << 5)
#define FF_WU_THS_1_THS6 (1 << 6)
#define FF_WU_THS_1_DCRM (1 << 7)

#define FF_WU_DURATION_1 0x33

#define I2C_SPEED_HZ 100000U
#define PCLK1_MHZ 16

/* direction of the accelerometer */
volatile int dir = 0;

/* Struct for a single task in the queue.
 * It can be either reading or writing request to the accelerometer. */
typedef struct {
    enum {
        QREAD, QWRITE
    } kind;
    char reg; /* register to read or write from */
    union {
        struct {
            enum {
                W_0, W_1, W_2, W_3, W_4
            } write_stage; /* current stage of the writing process */
            char value; /* value to be written to the register */
        };
        struct {
            enum {
                R_0, R_1, R_2, R_3, R_4, R_5, R_6,
            } read_stage; /* current stage of the reading process */
            void (*handler)(char); /* handler to be called when the value is
                                      read from the register */
        };
    };
} QueueTask;

/* Task status */
typedef enum {
    SAME, /* executing the same task */
    NEXT, /* finished current task */
    QERROR /* no action, something's wrong */
} TS;

typedef struct {
#define QSIZE 10
    QueueTask buf[QSIZE];
    /* active tasks are included in [beg, end)
    * when beg == end the queue is empty
    * when end + 1 == beg the queue is full */
    int beg, end;
} Queue;

/* Executing the reading task. */
static TS exec_rtask(QueueTask *task) {
    switch (task->read_stage) {
        case R_0: {
            I2C1->CR1 |= I2C_CR1_START;
            task->read_stage = R_1;
            return SAME;
        }
        case R_1: {
            if (I2C1->SR1 & I2C_SR1_SB) {
                I2C1->DR = LIS35DE_ADDR << 1;
                task->read_stage = R_2;
                return SAME;
            }
            break;
        }
        case R_2: {
            if (I2C1->SR1 & I2C_SR1_ADDR) {
                I2C1->SR2;
                I2C1->DR = task->reg;
                task->read_stage = R_3;
                return SAME;
            }
            break;
        }
        case R_3: {
            if (I2C1->SR1 & I2C_SR1_BTF) {
                I2C1->DR;
                I2C1->CR1 |= I2C_CR1_START;
                task->read_stage = R_4;
                return SAME;
            }
            break;
        }
        case R_4: {
            if (I2C1->SR1 & I2C_SR1_SB) {
                I2C1->DR = (LIS35DE_ADDR << 1) | 1U;
                task->read_stage = R_5;
                return SAME;
            }
            break;
        }
        case R_5: {
            if (I2C1->SR1 & I2C_SR1_ADDR) {
                I2C1->SR2;
                I2C1->CR1 &= ~I2C_CR1_ACK;
                task->read_stage = R_6;
                return SAME;
            }
            break;
        }
        case R_6: {
            if (I2C1->SR1 & I2C_SR1_RXNE) {
                I2C1->CR1 |= I2C_CR1_STOP;
                task->handler(I2C1->DR);
                return NEXT;
            }
        }
    }
    return QERROR;
}

/* Executing the write task. */
static TS exec_wtask(QueueTask *task) {
    switch (task->write_stage) {
        case W_0: {
            I2C1->CR1 |= I2C_CR1_START;
            task->write_stage = W_1;
            return SAME;
        }
        case W_1: {
            if (I2C1->SR1 & I2C_SR1_SB) {
                I2C1->DR = LIS35DE_ADDR << 1;
                task->write_stage = W_2;
                return SAME;
            }
            break;
        }
        case W_2: {
            if (I2C1->SR1 & I2C_SR1_ADDR) {
                I2C1->SR2;
                I2C1->DR = task->reg;
                task->write_stage = W_3;
                return SAME;
            }
            break;
        }
        case W_3: {
            if (I2C1->SR1 & I2C_SR1_TXE) {
                I2C1->DR = task->value;
                task->write_stage = W_4;
                return SAME;
            }
            break;
        }
        case W_4: {
            if (I2C1->SR1 & I2C_SR1_BTF) {
                I2C1->DR; /* clearing the BTF bit */
                I2C1->CR1 |= I2C_CR1_STOP;
                return NEXT;
            }
            break;
        }
    }
    return QERROR;
}

static Queue TQ = {.beg = 0, .end = 0};

/* executing arbitrary task */
static TS exec_task(Queue *Q) {
    QueueTask *task = &Q->buf[Q->beg];
    if (task->kind == QREAD)
        return exec_rtask(task);
    else
        return exec_wtask(task);
}

/* assuming that the queue is not empty */
static void pop_task(Queue *Q) {
    Q->beg = (Q->beg + 1) % QSIZE;
}

static int is_empty(Queue *Q) {
    return Q->beg == Q->end;
}

/* Adding a task to the queue */
static void add_task(Queue *Q, QueueTask *task) {
    int first = is_empty(Q);
    if ((Q->end + 1) % QSIZE == Q->beg) {
        /* TODO error handling */
    }
    else {
        Q->buf[Q->end] = *task;
        Q->end = (Q->end + 1) % QSIZE;
        if (first)
            exec_task(Q);
    }
}

/* Reading from the register 'reg' of the accelerometer.
 * When finished, call handler with the read value. */
static void read_acc(char reg, void (*handler)(char)) {
    QueueTask task;
    task.kind = QREAD;
    task.reg = reg;
    task.read_stage = R_0;
    task.handler = handler;
    add_task(&TQ, &task);
}

/* Writing value the the register of the accelerometer */
static void write_acc(char reg, char value) {
    QueueTask task;
    task.kind = QWRITE;
    task.reg = reg;
    task.value = value;
    task.write_stage = W_0;
    add_task(&TQ, &task);
}

/* handler for dealing with read left/right value from the
 * accelerometer */
static void acc_handler(char value) {
    signed char svalue = value;

    if (svalue >= 0)
        dir = 1;
    else
        dir = -1;
}

/* Configuring the I2C and the accelerometer. */
void init_accel() {
    /* configuring I2C */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
    GPIOafConfigure(GPIOB, 8, GPIO_OType_OD,
        GPIO_Low_Speed, GPIO_PuPd_NOPULL, 4);
    GPIOafConfigure(GPIOB, 9, GPIO_OType_OD,
        GPIO_Low_Speed, GPIO_PuPd_NOPULL, 4);
    I2C1->CR1 = 0;
    I2C1->CR2 = PCLK1_MHZ;
    I2C1->CCR = (PCLK1_MHZ * 1000000U) / (I2C_SPEED_HZ << 1);
    I2C1->TRISE = PCLK1_MHZ + 1U;
    /* enabling I2C interrupts */
    I2C1->CR2 |= I2C_CR2_ITEVTEN;
    I2C1->CR2 |= I2C_CR2_ITBUFEN;
    NVIC_EnableIRQ(I2C1_EV_IRQn);

    I2C1->CR1 |= I2C_CR1_PE;

    /* confguring the accelerometer */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    GPIOinConfigure(GPIOA, 1, GPIO_PuPd_UP, EXTI_Mode_Interrupt,
                    EXTI_Trigger_Rising);
    NVIC_EnableIRQ(EXTI1_IRQn);

    /* initializing the registers of the accelerometer */
    write_acc(CTRL_REG1, CTRL_REG1_PD | CTRL_REG1_Zen | CTRL_REG1_Xen |
                         CTRL_REG1_Yen);
    write_acc(CTRL_REG3, CTRL_REG3_I1CFG0);
    write_acc(FF_WU_CFG_1, FF_WU_CFG_1_YHIE);
    write_acc(FF_WU_THS_1, FF_WU_THS_1_THS4);
    write_acc(FF_WU_DURATION_1, 20);
}

/* IRQ handler for accelerometer moving left or right */
void EXTI1_IRQHandler() {
    EXTI->PR = EXTI_PR_PR1;
    read_acc(OUTY, acc_handler);
}

/* handler for I2C checking */
void I2C1_EV_IRQHandler() {
    Queue *Q = &TQ;
    if (is_empty(Q)) {
        /* TODO error handling */
        return;
    }

    switch (exec_task(Q)) {
        case SAME: {
            break;
        }
        case NEXT: {
            pop_task(Q);
            if (!is_empty(Q))
                exec_task(Q);
            break;
        }
        case QERROR: {
            /* TODO error handling */
            break;
        }
    }
}
