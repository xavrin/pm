#include <stm32.h>
#include <gpio.h>

#include "utils.h"
#include "main_timer.h"

#define TIM3_PSC 1600
#define ONE_MS 10
#define TIM3_ARR (50 * ONE_MS)

static void (*timer_handler)();

void TIM3_IRQHandler() {
    uint32_t tim_status = TIM3->SR & TIM3->DIER;

    if (tim_status & TIM_SR_UIF) {
        TIM3->SR = ~TIM_SR_UIF;
        timer_handler();
    }
}

void configure_main_timer(void (*handler)()) {
    timer_handler = handler;
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    TIM3->DIER = TIM_DIER_UIE;
    TIM3->PSC = TIM3_PSC;
    TIM3->ARR = TIM3_ARR;
    NVIC_EnableIRQ(TIM3_IRQn);
    TIM3->CR1 = TIM_CR1_CEN;
}
