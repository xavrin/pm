#include <stm32.h>
#include <gpio.h>
#include <string.h>
#include "accel.h"
#include "utils.h"
#include "buttons.h"
#include "game.h"
#include "lcd.h"
#include "main_timer.h"
#include "sleep.h"

#define MAIN_LOOP_SLEEP_MS 25

Game game; /* game object */

/* Variable that forces a step in game. It is set to 1 by the timer handler
 * and should be cleared in the loop in main. */
static volatile int step = 0;

/* Updating the status of the game with the buttons pushed by the user.
 * This function should be called with interrupts disabled. */
void check_buttons(Game *game) {
    if (fire)
        fire_bullet(game);
    fire = 0;
    if (dir > 0)
        move_right(game);
    else if (dir < 0)
        move_left(game);
}

/* Draw the board on the lcd. */
void draw() {
    static char board[SIZEX][SIZEY];

    draw_board(&game, board);
    LCDdrawBoard(board);
    LCDdrawPoints(get_points(&game));
}

/* Making the step in game.
 * Starting a new game iff the player has lost.
 * Redraw the board if something has changed. */
void update_game() {
    irq_level_t level;
    int update = 0;

    level = IRQprotectAll();
    if (step) {
        update = 1;
        step = 0;
    }
    IRQunprotectAll(level);

    if (update) {

        level = IRQprotectAll();
        check_buttons(&game);
        IRQunprotectAll(level);

        make_step(&game);

        if (game.status == LOST) {
            init_game(&game);
            LCDclear();
        }
        draw();
    }
}

void main_timer_handler() {
    step = 1;
}

int main() {
    init_usart();
    init_accel();
    init_buttons();
    init_game(&game);
    configure_main_timer(main_timer_handler);
    configure_sleep();
    LCDconfigure();
    LCDclear();

    while (1) {
        update_game();
        sleep(MAIN_LOOP_SLEEP_MS);
    }
    return 0;
}
