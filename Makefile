CC = arm-eabi-gcc
OBJCOPY = arm-eabi-objcopy
FLAGS = -mthumb -mcpu=cortex-m4 \
		-mfloat-abi=softfp -mfpu=fpv4-sp-d16
CPPFLAGS = -DSTM32F411xE
CFLAGS = $(FLAGS) -Wall -g -I/opt/arm/stm32/inc \
		-I/opt/arm/stm32/CMSIS/Include \
		-I/opt/arm/stm32/CMSIS/Device/ST/STM32F4xx/Include \
		-O2 -ffunction-sections -fdata-sections
LDFLAGS = $(FLAGS) -Wl,--gc-sections -nostartfiles \
		-L/opt/arm/stm32/lds -Tstm32f411re.lds

vpath %.c /opt/arm/stm32/src
.PHONY: all clean
.SECONDARY: main.elf main.o startup_stm32.o gpio.o delay.o fonts.o

all: main.bin

accel.o: accel.h utils.h

utils.o: utils.h

buttons.o: utils.h buttons.h

game.o: game.h board.h

lcd.o: lcd.h board.h

main_timer.o: main_timer.h

sleep.o : sleep.h

main.elf : main.o startup_stm32.o gpio.o delay.o utils.o \
			accel.o buttons.o game.o lcd.o fonts.o main_timer.o \
			sleep.o
	$(CC) $(LDFLAGS) $^ -o $@

%.bin : %.elf
	$(OBJCOPY) $< $@ -O binary

clean :
	rm -f *.bin *.elf *.hex *.d *.o *.bak *~
