#ifndef __BOARD__H__
#define __BOARD__H__

/* size of the board */
#define SIZEX 64
#define SIZEY 71

/* Constants for the symbols on the board */
#define BOARDC 0
#define BATTLESHIPC 1
#define BULLETC 2
#define ROCKC 3

#endif
