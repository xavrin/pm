#ifndef __BUTTONS__H__
#define __BUTTONS__H__

/* Variables for signalizing that some buttons were pushed.
 * They should be cleared after reading in order not to
 * react twice to the same action. */
volatile int fire;

void EXTI15_10_IRQHandler();
void TIM4_IRQHandler();

void init_buttons();

#endif
