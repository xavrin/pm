#ifndef __ACCEL__H__
#define __ACCEL__H__

volatile int dir; /* direction of the accelerometer */

void init_accel();

void EXTI1_IRQHandler();
void I2C1_EV_IRQHandler();

#endif
