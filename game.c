#include "game.h"
#define SWAP(x, y) do { typeof(x) tmp = x; x = y; y = tmp; } while (0)

static char board[SIZEX][SIZEY];

static int is_point_ok(int x, int y) {
    return x >= 0 && x < SIZEX && y >= 0 && y < SIZEY;
}

static int is_obj_ok(Pos l, Size s) {
    return is_point_ok(l.x, l.y) && is_point_ok(l.x + s.x - 1, l.y + s.y - 1);
}

static int intersecting(Pos p1, Size s1, Pos p2, Size s2) {
    int i, j;
    int ok = 0;
    for (i = p1.x; i < p1.x + s1.x; i++)
        for (j = p1.y; j < p1.y + s1.y; j++)
            if (p2.x <= i && i < p2.x + s2.x && p2.y <= j && j < p2.y + s2.y)
                ok = 1;
    return ok;
}

static void destroy_bullet(Game *game, int bullet_nr) {
    SWAP(game->bullets[bullet_nr], game->bullets[game->bullets_num-1]);
    game->bullets_num--;
}

static unsigned rand(Game *game) {
    static unsigned P = 143513453;
    static unsigned Q = 34534535;
    Q++;
    P = P * (game->fired + game->points + game->bullets_num +
             game->rocks_num + game->b.dir) + Q;
    return P;
}

static int has_enough_space(Game *game, char board[SIZEX][SIZEY], Pos pos,
                            Size size) {
    int i, j;
    int ok = 1;
    for (i = pos.x; i < pos.x + size.x; i++)
        for (j = pos.y; j < pos.y + size.y; j++)
            if (board[i][j])
                ok = 0;
    return ok;

}

static void clear_board(Game *game, char board[SIZEX][SIZEY]) {
    int i, j;
    for (i = 0; i < SIZEX; i++)
        for (j = 0; j < SIZEY; j++)
            board[i][j] = 0;
}

static void draw_rocks(Game *game, char board[SIZEX][SIZEY], int nr) {
    int i, j, k;
    for (k = 0; k < game->rocks_num; k++) {
        Rock *rock = &game->rocks[k];
        for (i = rock->pos.x; i < rock->pos.x + rock->size.x; i++)
            for (j = rock->pos.y; j < rock->pos.y + rock->size.y; j++)
                board[i][j] = (nr) ? (k + 1) : (ROCKC + rock->color);
    }
}

static void draw_bullets(Game *game, char board[SIZEX][SIZEY]) {
    int k;
    for (k = 0; k < game->bullets_num; k++) {
        Bullet *bull = &game->bullets[k];
        board[(int)bull->pos.x][(int)bull->pos.y] = BULLETC;
    }
}

static void draw_battleship(Game *game, char board[SIZEX][SIZEY]) {
    int i, j;
    Battleship *b = &game->b;
    for (i = b->pos.x; i < b->pos.x + b->size.x; i++)
        for (j = b->pos.y; j < b->pos.y + b->size.y; j++)
            board[i][j] = BATTLESHIPC;
}

static void add_new_rock(Game *game) {
    clear_board(game, board);
    draw_rocks(game, board, 1);
    draw_bullets(game, board);

    if (game->rocks_num < MAX_ROCKS && rand(game) % 2) {
        Pos pos;
        Size size;
        pos.x = rand(game) % (SIZEX - RXSIZE + 1);
        pos.y = SIZEY - RYSIZE;
        size.x = RXSIZE;
        size.y = RYSIZE;
        if (has_enough_space(game, board, pos, size)) {
            Rock *rock = &game->rocks[(int)(game->rocks_num++)];
            rock->pos = pos;
            rock->size = size;
            rock->destroyed = 0;
            rock->speed.val = 0;
            rock->speed.max = ROCK_STEP_MIN + rand(game) % (ROCK_STEP_MAX - ROCK_STEP_MIN + 1);
            rock->color = rand(game) % 10;
        }
    }
}

static void fire_bullets(Game *game) {
    if (game->fired) {
        game->fired = 0;
        int bposx = game->b.pos.x + FIREPOSX;
        int bposy = game->b.pos.y + FIREPOSY;
        Bullet *bull = &game->bullets[game->bullets_num++];
        bull->pos.x = bposx;
        bull->pos.y = bposy;
        bull->speed.val = 0;
        bull->speed.max = BULLET_STEP;
    }
}

static int should_move(Speed *speed) {
    speed->val++;
    if (speed->val == speed->max) {
        speed->val = 0;
        return 1;
    }
    else
        return 0;
}

static void move_battleship(Game *game) {
    Battleship *b = &game->b;
    if (b->dir != NONE && should_move(&b->speed)) {

        Pos npos = b->pos;
        npos.x += 1 - 2 * (b->dir == LEFT);
        if (is_obj_ok(npos, b->size))
            b->pos.x = npos.x;
        b->dir = NONE;
    }
}

static void move_bullets(Game *game) {
    int i, k;
    int rock_nr;
    /* creating the map of rocks */
    clear_board(game, board);
    draw_rocks(game, board, 1);

    k = 0;
    while (k < game->bullets_num) {
        int destroyed = 0;
        Bullet *bul = &game->bullets[k];
        /* something should be destroyed, we assume that rock size
         * is greater thatn one */
        if (should_move(&bul->speed)) {
            for (i = 0; i <= 1; i++)
                if (bul->pos.y + i < SIZEY
                        && (rock_nr = board[(int)bul->pos.x][(int)bul->pos.y + i])) {
                    rock_nr--;
                    if (!game->rocks[rock_nr].destroyed)
                        game->points += SHOOTING_ROCK_POINTS;
                    game->rocks[rock_nr].destroyed = 1;
                    destroy_bullet(game, k);
                    destroyed = 1;
                    break;
                }
        }
        if (!destroyed) {
            if (bul->pos.y == SIZEY - 1)
                destroy_bullet(game, k);
            else {
                bul->pos.y++;
                k++;
            }
        }
    }
}

static void cleanup_rocks(Game *game) {
    int i, j;
    i = 0; j = game->rocks_num - 1;
    while (i < j) {
        while (i < game->rocks_num && !game->rocks[i].destroyed)
            i++;
        while (j >= 0 && game->rocks[j].destroyed) {
            j--;
            game->rocks_num--;
        }
        if (i < j) {
            SWAP(game->rocks[i], game->rocks[j]);
            i++;
            j--;
            game->rocks_num--;
        }
    }
}

static void move_rocks(Game *game) {
    int k;
    Battleship *b = &game->b;

    for (k = 0; k < game->rocks_num; k++) {
        Rock *rock = &game->rocks[k];
        if (should_move(&rock->speed)) {
            if (rock->pos.y > 0) {
                rock->pos.y--;
                if (intersecting(rock->pos, rock->size,
                                b->pos, b->size)) {
                    game->status = LOST;
                }
            }
            else {
                if (!rock->destroyed) {
                    game->status = LOST;
                }
                rock->destroyed = 1;
            }
        }
    }
    cleanup_rocks(game);
}

void make_step(Game *game) {
    fire_bullets(game);
    move_battleship(game);
    move_rocks(game);
    move_bullets(game);
    add_new_rock(game);
}

void draw_board(Game *game, char board[SIZEX][SIZEY]) {
    clear_board(game, board);
    draw_rocks(game, board, 0);
    draw_bullets(game, board);
    draw_battleship(game, board);
}

int get_points(Game *game) {
    return game->points;
}

void init_game(Game *game) {
    game->status = PLAYING;
    game->rocks_num = 0;
    game->bullets_num = 0;
    game->b.size.x = BXSIZE;
    game->b.size.y = BYSIZE;
    game->b.pos.x = 0;
    game->b.pos.y = 0;
    game->b.dir = NONE;
    game->fired = 0;
    game->b.speed.val = 0;
    game->b.speed.max = BATTLESHIP_STEP;
    game->points = 0;
}

void move_left(Game *game) {
    game->b.dir = LEFT;
}

void move_right(Game *game) {
    game->b.dir = RIGHT;
}

void fire_bullet(Game *game) {
    game->fired = 1;
}
