#ifndef __SLEEP__H__
#define __SLEEP__H__

void sleep(unsigned time);
void configure_sleep();
void TIM2_IRQHandler();

#endif
