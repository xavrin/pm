#include <gpio.h>
#include <stm32.h>

#include "sleep.h"
#include "utils.h"

#define TIM2_PSC 159
/* Value corresponding to one ms of sleep.
 * Should be used to express an arbitrary number of ms */
#define ONE_MS 100

static volatile int wait = 0;

void sleep(unsigned time) {
    if (time > 0) {
        wait = 1;

        TIM2->ARR = time * ONE_MS;
        TIM2->EGR = TIM_EGR_UG;

        TIM2->CR1 |= TIM_CR1_CEN;
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
        do {
            __WFE();
        } while (wait);
    }
}

void TIM2_IRQHandler() {
    uint32_t tim_status = TIM2->SR & TIM2->DIER;
    if (tim_status & TIM_SR_UIF) {
        TIM2->SR = ~TIM_SR_UIF;
        TIM2->CR1 &= ~TIM_CR1_CEN;
        wait = 0;
    }
}

void configure_sleep() {
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    TIM2->DIER = TIM_DIER_UIE;
    TIM2->PSC = TIM2_PSC;
    TIM2->CR1 = TIM_CR1_URS;
    NVIC_EnableIRQ(TIM2_IRQn);
}
