#include <stm32.h>
#include <gpio.h>
#include "utils.h"
#include "buttons.h"

/* Indexes of the buttons */
#define J_FIRE 0 /* Joystick fire button */
#define BNUM 1 /* Number of buttons */

/* By default frequency of the timer is 16MHz.
 * The constants below set the 4th timer to count to 20ms. */
#define TIM4_PSC 1000
#define TIM4_ARR 320

volatile int fire = 0;

typedef enum {
    IDLE, /* No action. */
    STARTED /* Button pushed, waiting for timer to check whether it's still
               pushed. */
} ButtonStatus;

typedef struct button {
    GPIO_TypeDef *addr; /* Address of GPIOA, GPIOB, etc. */
    int shift; /* Number of bit which corresponds to the given button. */
    const char *name; /* Name of the button. */
    int pushed_val; /* Value which means that the button is pushed.
                       It is zero or one. */
    volatile int *state; /* Address of a variable to which we should raport
                            that the button was pushed. */
    ButtonStatus status; /* Status of the button */
} button;

static button buttons[BNUM] = {
    {GPIOB, 10, "FIRE", 0, &fire, IDLE},
};

/* Check whether te button is currently being pushed in. */
static int button_in(int id) {
    int val = !!(buttons[id].addr->IDR & (1 << buttons[id].shift));
    return val == buttons[id].pushed_val;
}

/* Action when the button was pushed.
 * We take the contact bounce into consideration. */
static void mark_button_pushed(int id) {
    *(buttons[id].state) = 1;
}

/* Button was puhsed in for the first time.
 * Start the timer to check if it's stable. */
static void button_start(int id) {
    if (button_in(id)) {
        buttons[id].status = STARTED;
        TIM4->CNT = 0;
        TIM4->CR1 |= TIM_CR1_CEN;
    }
}

/* Update the state of the button.
 * It's called after the timer stops. */
static void button_check(int id) {
    if (button_in(id) && buttons[id].status == STARTED) {
        buttons[id].status = IDLE;
        mark_button_pushed(id);
    }
}

/* Check all of the buttons. */
static void buttons_lookup() {
    int i;
    for (i = 0; i < BNUM; i++)
        button_check(i);
}

void EXTI15_10_IRQHandler() {
    uint32_t status = EXTI->PR;
    if (status & EXTI_PR_PR10) {
        button_start(J_FIRE);
        EXTI->PR = EXTI_PR_PR10;
    }
}

void TIM4_IRQHandler() {
    uint32_t tim_status = TIM4->SR & TIM4->DIER;
    if (tim_status & TIM_SR_UIF) {
        TIM4->SR = ~TIM_SR_UIF;
        TIM4->CR1 &= ~TIM_CR1_CEN;
        buttons_lookup();
    }
}

/* Init buttons and a necessary timer. */
void init_buttons() {
    int i;
    /* configuring the timer for interrupts */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    TIM4->DIER = TIM_DIER_UIE;
    TIM4->PSC = TIM4_PSC;
    TIM4->ARR = TIM4_ARR;
    NVIC_EnableIRQ(TIM4_IRQn);

    /* configuring buttons */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN |
                    RCC_AHB1ENR_GPIOBEN |
                    RCC_AHB1ENR_GPIOCEN;

    for (i = 0; i < BNUM; i++) {
            GPIOinConfigure(buttons[i].addr,
                            buttons[i].shift,
                            GPIO_PuPd_NOPULL,
                            EXTI_Mode_Interrupt,
                            (buttons[i].pushed_val) ? EXTI_Trigger_Rising
                                                    : EXTI_Trigger_Falling);
    }
    NVIC_EnableIRQ(EXTI15_10_IRQn);
}
